import React from "react";
import { Route } from "react-router-dom";

import AdList from './containers/AdListView'
import AdDetail from './containers/AdDetailView'
import AdCreateUpdate from './containers/AdCreateUpdate'
import Login from './containers/Login'
import Register from './containers/Register'


const BaseRouter = () =>(
    <div>
        <Route exact path="/" component={AdList} />
        <Route exact path="/ad/:uuid" component={AdDetail} />
        <Route exact path="/ad/ad/create/" component={AdCreateUpdate} />
        <Route exact path="/ad/:uuid/update/" component={AdCreateUpdate} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
    </div>

);

export default BaseRouter;