import React from "react";
import { connect } from "react-redux";
import AdForm from '../components/adForm'
import axios from "axios";

class AdCreateUpdate extends React.Component{
    state = {
        ad: [],
        error: null
    };

    getAd = () => {
        const uuid = this.props.match.params.uuid;
        if (uuid) {
            axios.get(`http://127.0.0.1:8000/ad/${uuid}`, {withCredentials: true}).then(res => {
                this.setState({
                    ad: res.data
                });
            });
        }
    }

    componentDidMount() {
        this.getAd();
    }

    render() {
        if (this.props.token !==null) {
            return (
                <div>
                    <AdForm data={this.state.ad}/>
                </div>
            )
        }else{
            return(
                <div>
                    <p>Please <a href={'/login'}>Login</a> or <a href={'/signup'}>Sign up</a></p>
                </div>
            )
        }
    }
}

const mapStateToProps = state => {
  return {
    token: state.token
  };
};

export default connect(mapStateToProps)(AdCreateUpdate);
