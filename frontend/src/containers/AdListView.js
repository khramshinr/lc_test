import React from 'react';
import axios from 'axios';

import Ad from '../components/ad'


class AdList extends React.Component{
    state = {
        ad: []
    };

    getAd = () => {
        axios.get("http://127.0.0.1:8000/ad/", {withCredentials: true}).then(res => {
            this.setState({
                ad: res.data
            });
        });
    }

    componentDidMount() {
        this.getAd();
    }

    render() {
        return(
            <div>
                <Ad data={this.state.ad}/>
            </div>
        )
    }
}

export default AdList
