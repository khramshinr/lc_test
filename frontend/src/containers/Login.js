import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions/auth';
import {Form, Icon, Input, Button, message} from 'antd';


class NormalLoginForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onAuth(values.userName, values.password);
      }
    });
  }

  render() {
    if (this.props.error) {
        for (var key in this.props.error){
            message.error(this.props.error[key].join('\n'));
        }
        this.props.clearError()
    }

    const { getFieldDecorator } = this.props.form;
    return (
        <div>
          <Form onSubmit={this.handleSubmit} className="login-form">

            <Form.Item>
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: 'Please input your username!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
              )}
            </Form.Item>

            <Form.Item>
              {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
              )}
            </Form.Item>

            <Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Log in
              </Button>
              Or <a href="/register">register now!</a>
            </Form.Item>

          </Form>
        </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(NormalLoginForm);

const mapStateToProps = (state) => {
    return {
        error: state.error
    }
}


const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, password) => dispatch(actions.authLogin(username, password)),
        clearError:() => dispatch(actions.authClearError())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm);