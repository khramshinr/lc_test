import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import {Button, Card, message} from "antd";

const { Meta } = Card

class AdDetail extends React.Component{
    state = {
        ad: [],
        error: null
    };

    getAd = () => {
        const uuid = this.props.match.params.uuid;
        axios.get(`http://127.0.0.1:8000/ad/${uuid}`, {withCredentials: true}).then(res => {
            this.setState({
                ad: res.data
            });
        });
    }

    handleDelete = event =>{
        event.preventDefault();
        axios.defaults.headers = {
            "Content-Type": "application/json",
            Authorization: `Token ${this.props.token}`
        };
        axios.delete(`http://127.0.0.1:8000/ad/${this.state.ad.uuid}`)
            .then(res => {
                if (res.status === 204) {
                    window.location = '/'
                }
            })
            .catch(error => {
                message.error(error.response.statusText)
            });

    }

    componentDidMount() {
        this.getAd();
    }

    render() {
        return(
          <div style={{ background: '#ECECEC', padding: '30px' }}>
              <Card title={this.state.ad.title} bordered={false} style={{ width: '90%' }}>
                <Meta
                  description={this.state.ad.description}
                />
              </Card>
              <Button type='primary' htmlType='submit'>
                  <a href={`/ad/${this.state.ad.uuid}/update`}>Update</a>
              </Button>
              <form onSubmit={this.handleDelete}>
                  <Button type='danger' htmlType='submit'>
                      Delete
                  </Button>
              </form>
          </div>
        )
    }
}

const mapStateToProps = state => {
  return {
    token: state.token
  };
};

export default connect(mapStateToProps)(AdDetail);
