import axios from 'axios'

import * as actionTypes from './actionTypes'


export  const  authClearError = () =>{
    return {
        type: actionTypes.AUTH_CLEAR_ERROR
    }
}

export const authStart = () =>{
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = token =>{
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token
    }
}

export const authFail = error =>{
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const authLogout = () =>{
    localStorage.removeItem('token');
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const authLogin = (username, password) =>{
    return dispatch =>{
        dispatch(authStart());
        axios.post('http://127.0.0.1:8000/rest-auth/login/', {
            username: username,
            password: password
        })
            .then(res =>{
                const token = res.data.key
                localStorage.setItem('token', token)
                dispatch(authSuccess(token))
                window.location = '/'
            })
            .catch(error =>{
                dispatch(authFail(error.response.data))
            })
    }
}

export const authSignup = (username, email, password1, password2) =>{
    return dispatch =>{
        dispatch(authStart());
        axios.post('http://127.0.0.1:8000/rest-auth/registration/', {
            username: username,
            email: email,
            password1: password1,
            password2: password2
        })
            .then(res =>{
                const token = res.data.key
                localStorage.setItem('token', token)
                dispatch(authSuccess(token))
                window.location = '/'
            })
            .catch((error) =>{
                dispatch(authFail(error.response.data))
            })
    }
}


export const authCheckState = () =>{
    return dispatch =>{
        const token = localStorage.getItem('token');
        if (token === undefined){
            dispatch(authLogout());
        }else{
            dispatch(authSuccess(token));
        }
    }
}


