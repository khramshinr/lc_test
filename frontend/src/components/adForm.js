import React from 'react'
import { Form, Select, Input, Button } from 'antd';
import { connect } from "react-redux";
import axios from "axios";


const { TextArea } = Input;


class AdForm extends React.Component {
    state = {
        cityList: []
    };

    getCityList = () => {
        axios.get("http://127.0.0.1:8000/ad/city", {withCredentials: true}).then(res => {
            this.setState({
                cityList: res.data
            });
        });
    }

    handleSubmit = (event, uuid) => {
        event.preventDefault();
        const postObj = {
            user: this.props.data.city ? this.props.data.city : null,
            title: event.target.elements.title.value,
            description: event.target.elements.description.value,
            city: this.props.data.city
        }

        axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
        axios.defaults.xsrfCookieName = "csrftoken";
        axios.defaults.headers = {
            "Content-Type": "application/json",
            Authorization: `Token ${this.props.token}`,
        };

        this.props.form.validateFields((err, values) => {
            if (!err) {
                if(uuid){
                    axios.put(`http://127.0.0.1:8000/ad/${uuid}`, postObj)
                        .then(res => {
                            if (res.status === 200) {
                                window.location = '/'
                            }
                        });
                }
                else{
                    axios.post("http://127.0.0.1:8000/ad/create", postObj)
                        .then(res => {
                            if (res.status === 201) {
                                window.location = '/'
                            }
                        })
                }
            }
        });
    }

    setFormOldData =() =>{
        if (this.props.data.uuid){
            this.props.form.setFieldsInitialValue({
                title: `${this.props.data.title}`,
                description: `${this.props.data.description}`,
                city: `${this.props.data.city}`,
            });
        }
    }

    componentDidMount() {
        this.getCityList();
    }

    handleSelectChange = (value) => {
        this.props.data.city=value
    }

    render() {
        this.setFormOldData()
        const { getFieldDecorator } = this.props.form;

        return (
            <Form onSubmit={event => (this.handleSubmit(event, this.props.data.uuid))}>
                <Form.Item
                    label="Title"
                    labelCol={{ span: 5 }}
                    wrapperCol={{ span: 12 }}
                >
                    {getFieldDecorator('title', {
                        rules: [{ required: true, message: 'Please input your title!' }],
                    })(
                        <Input name='title' />
                    )}
                </Form.Item>

                <Form.Item
                    label="Description"
                    labelCol={{ span: 5 }}
                    wrapperCol={{ span: 12 }}
                >
                    {getFieldDecorator('description')(
                        <TextArea  name='description' rows={4}/>
                    )}
                </Form.Item>

                <Form.Item
                    label="City"
                    labelCol={{ span: 5 }}
                    wrapperCol={{ span: 12 }}
                >
                {getFieldDecorator('city', {
                    rules: [{ required: true, message: 'Please select your gender!' }],
                })(
                    <Select
                        placeholder="Select a option and change input text above"
                        onChange={this.handleSelectChange}
                    >
                        {this.state.cityList.map(function(a) {
                            return (
                            <option key={a.id} val={a.id}>{a.name}</option>
                        );
                        })}
                    </Select>
                )}
                </Form.Item>

                <Form.Item
                    wrapperCol={{ span: 12, offset: 5 }}
                >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

const WrappedApp = Form.create({ name: 'coordinated' })(AdForm);

const mapStateToProps = state => {
  return {
    token: state.token
  };
};

export default connect(mapStateToProps)(WrappedApp);
