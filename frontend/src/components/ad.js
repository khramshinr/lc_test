import React from 'react'
import { Table } from 'antd';

class Ad extends React.Component {
    columns = [{
            title: 'Title',
            dataIndex: 'title',
            render: (text, record) => <a href={`/ad/${record.uuid}`}>{text}</a>,
        },
        {
            title: 'User',
            dataIndex: 'user',
        },
        {
            title: 'City',
            dataIndex: 'city',
        }
    ];

    render() {
        return(
            <Table rowKey={record => record.uuid} columns={this.columns} dataSource={this.props.data}/>
        )
    };

}

export default Ad;