from django.conf.urls import url

from .views import AdListView
from .views import AdDetailView
from .views import CityListView

app_name = 'ad'

urlpatterns = [
    url(r'^$', AdListView.as_view(), name='ad'),
    url(r'^city$', CityListView.as_view(), name='city'),
    url(r'^create$', AdDetailView.as_view({'post': 'create'}), name='adcreate'),
    url(r'^(?P<uuid>.+)$', AdDetailView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}), name='addetail'),

]
