from uuid import uuid4

from django.db import models
from django.contrib.auth.models import User


class City(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True, help_text='City name.')

    def __str__(self):
        return self.name


class Ad(models.Model):
    uuid = models.UUIDField(default=uuid4, db_index=True, unique=True, editable=False, help_text='UUID for this ad.')
    title = models.CharField(max_length=255, blank=True, null=True, help_text='Ad title.')
    description = models.TextField(null=True, blank=True, help_text='Ad description.')
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='ads', help_text='City of this ad.')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='ads', help_text='Owner of this ad.')
    unique_views = models.PositiveIntegerField(default=0, help_text='Count of unique ad views')
    total_views = models.PositiveIntegerField(default=0, help_text='Total ad views')
    created_on = models.DateTimeField(auto_now_add=True, help_text='Ad creation time.')
    updated_on = models.DateTimeField(auto_now=True, help_text='Last modification time.')



