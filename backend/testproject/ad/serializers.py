from django.db.models import F

from rest_framework import serializers

from .models import Ad
from .models import City


class AdListSerializer(serializers.ModelSerializer):
    user = serializers.SlugRelatedField(slug_field='username', read_only=True, help_text='Owner of this note.')
    city = serializers.SlugRelatedField(slug_field='name', read_only=True, help_text='Owner of this note.')

    class Meta:
        model = Ad
        fields = ['uuid', 'title', 'user', 'city']


class AdDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ad
        fields = ['uuid', 'title', 'description', 'user', 'city', 'created_on', 'updated_on']

    def to_representation(self, instance):
        if str(instance.uuid) in self.context['request'].COOKIES:
            Ad.objects.filter(uuid=instance.uuid).update(total_views=F('total_views') + 1)
        else:
            Ad.objects.filter(uuid=instance.uuid).update(total_views=F('total_views') + 1, unique_views=F('unique_views')+1)
        return super(AdDetailSerializer, self).to_representation(instance)

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        instance = super(AdDetailSerializer, self).create(validated_data)
        return instance


class CityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ['id', 'name']
