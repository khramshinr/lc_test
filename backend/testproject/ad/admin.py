from django.contrib import admin

from .models import Ad
from .models import City


class AdAdmin(admin.ModelAdmin):
    list_display = ('uuid', 'title', 'description', 'city', 'user', 'created_on', 'updated_on')


class CityAdmin(admin.ModelAdmin):
    list_display = ('name',)


admin.site.register(Ad, AdAdmin)
admin.site.register(City, CityAdmin)

