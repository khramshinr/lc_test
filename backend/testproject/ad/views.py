from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet


from .models import Ad
from .models import City
from .serializers import AdListSerializer
from .serializers import AdDetailSerializer
from .serializers import CityListSerializer


class AdListView(ListAPIView):
    queryset = Ad.objects.all()
    serializer_class = AdListSerializer


class AdDetailView(ModelViewSet):
    queryset = Ad.objects.all()
    serializer_class = AdDetailSerializer
    lookup_field = "uuid"

    def retrieve(self, request, *args, **kwargs):
        response = super(AdDetailView, self).retrieve(request, *args, **kwargs)
        response.set_cookie(response.data['uuid'], True)
        return response


class CityListView(ListAPIView):
    queryset = City.objects.all()
    serializer_class = CityListSerializer



