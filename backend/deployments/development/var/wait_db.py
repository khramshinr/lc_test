import sys
import argparse
import time
import psycopg2


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--database', default='testproject')
    parser.add_argument('-u', '--user', default='testproject')
    parser.add_argument('-p', '--password', default='developmentpassword')
    parser.add_argument('-s', '--host')
    parser.add_argument('-w', '--port')

    return parser


def main():
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])

    print('waiting DB connection...')
    start = False
    while not start:
        try:
            conn = psycopg2.connect(database=namespace.database, user=namespace.user, password=namespace.password, host=namespace.host, port=namespace.port)
            start = True
            print('DB connection successful')
        except psycopg2.OperationalError:
            print('Trying again in 5 seconds...')
            time.sleep(5)


if __name__ == '__main__': main()